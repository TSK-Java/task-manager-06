package ru.tsc.kirillov.tm;

import ru.tsc.kirillov.tm.constant.ArgumentConst;
import ru.tsc.kirillov.tm.constant.TerminalConst;

import java.util.Scanner;

public final class Application {

    public static void main(final String[] args) {
        if (processArgument(args))
            close();

        showWelcome();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("\nВведите команду:");
            final String cmdText = scanner.nextLine();
            processCommand(cmdText);
        }
    }

    private static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0)
            return false;

        final String firstArg = args[0];
        processArgument(firstArg);

        return true;
    }

    private static void processCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty())
            return;
        switch (cmd) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                showUnexpectedCommand(cmd);
                break;
        }
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty())
            return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showUnexpectedArgument(arg);
                break;
        }
    }

    public static void close() {
        System.exit(0);
    }

    public static void showWelcome() {
        System.out.println("** Добро пожаловать в Task Manager **");
    }

    private static void showAbout() {
        showTerminalHeader(TerminalConst.ABOUT);
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

    private static void showHelp() {
        showTerminalHeader(TerminalConst.HELP);
        System.out.printf("%s, %s - Отображение информации о разработчике.\n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s - Отображение версии программы.\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - Отображение доступных команд.\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s - Закрытие приложения.\n", TerminalConst.EXIT);
    }

    private static void showVersion() {
        showTerminalHeader(TerminalConst.VERSION);
        System.out.println("1.6.0");
    }

    private static void showUnexpectedCommand(final String cmd) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Команда `%s` не поддерживается.\n", cmd);
    }

    private static void showUnexpectedArgument(final String arg) {
        showTerminalHeader(TerminalConst.ERROR);
        System.err.printf("Аргумент `%s` не поддерживается.\n", arg);
    }

    private static void showTerminalHeader(final String name) {
        System.out.printf("[%s]\n", name != null ? name.toUpperCase() : "N/A");
    }

}
